segment .text
extern	__errno_location
global ft_write

; int ft_write(int rdi, const void *rsi, size_t rdx);

ft_write:
    		mov 	rax, 0x1
			syscall
			cmp 	rax, 0
			jge		end

error:
			neg		rax
			push 	rax
			call 	__errno_location
			mov 	r10, rax
			pop 	rax
			mov 	[r10], rax
			mov 	rax, -1

end:
			ret
